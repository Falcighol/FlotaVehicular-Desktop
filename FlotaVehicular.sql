﻿CREATE DATABASE FlotaVehicular;
USE FlotaVehicular;

--
-- Estructura de tabla para la tabla `Vehículos`
--

CREATE TABLE IF NOT EXISTS Vehiculos
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    fechaAdquisicion VARCHAR(20) NOT NULL,
    taller BOOLEAN NOT NULL,
    modelo VARCHAR(50) NOT NULL,
    placa VARCHAR(10) UNIQUE NOT NULL,
    marca VARCHAR(50) NOT NULL,
    kilometrosRecorridos DOUBLE NOT NULL,
    fechaUltimoCambioAceite VARCHAR(20) NOT NULL,
    fechaUltimoMantenimiento VARCHAR(20) NOT NULL,
    observaciones VARCHAR(200) NOT NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT
INTO
    vehiculos
VALUES
(
    null, '17/01/2017', false, 'Tercel', '321 ABC', 'Toyota', 
    44.7, '09/04/2018', '09/04/2018', 'Sin observaciones', 1
),
(
    null, '16/01/2017', false, 'Corolla', '213 BCA', 'Toyota', 
    99.0, '02/03/2018', '02/03/2018', 'Necesita un cambio de motor', 1
),
(
    null, '03/03/2017', true,  'Lancer', '321 BCA', 'Mitsubishi', 
    78.6, '09/03/2918', '09/03/2918', 'Sin observaciones', 1
),
(
    null, '01/01/2017', false, 'Juke', '456 CBA', 'Nissan', 
    55.6, '06/05/2018', '06/05/2018', 'Fuga de aceite', 1
),
(
    null, '31/07/2017', false, 'Hardbody', '654 BCA', 'Nissan', 
    44.5, '07/07/2018', '07/07/2018', 'Sin observaciones', 1
),
(
    null, '03/07/2017', false, 'Nativa', '645 BCA', 'Jeep',
    88.7, '06/04/2018', '06/04/2018', 'Sin observaciones', 1
),
(
    null, '01/04/2016', false, 'Outlander', '789 BCA', 'Mitsubishi', 
    99.0, '06/02/2018', '06/02/2018', 'Sin observaciones', 1
),
(
    null, '05/05/2017', false, 'Eclipse', '897 BAC', 'Mitsubishi', 
    77.8, '04/02/2018', '04/02/2018', 'Sin observaciones', 1
),
(
    null, '08/09/2017', false, 'Montero', '798 ABC', 'Mitsubishi', 
    44.5, '27/02/2018', '27/02/2018', 'Sin observaciones', 1
),
(
    null, '17/11/2016', false, 'Civic', '123 ABC', 'Honda', 
    55.8, '10/08/2018', '10/08/2018', 'Sin observaciones', 1
);

-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `Talleres`
--
CREATE TABLE IF NOT EXISTS Talleres
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    telefono VARCHAR(15) UNIQUE NOT NULL,
    fallas VARCHAR(200) NOT NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT
INTO
    Talleres
VALUES
(
    null, 'Ridge', 'Santa Tecla', '4724-5135', 'Motor, Neumaticos, Filtros', 1
),
(
    null, 'Real Pro Auto Service', '7246-5135', 'Colonia Escalon', 'Suspencion, Frenos, Luces', 1
),
(
    null, 'Star Motors', 'Lourdes', '9783-5135', 'Cambio de aceite, Lavado de motor, Bujias', 1
),
(
    null, 'Auto Garage', 'Santa Lima', '6924-5135', 'Revision de sistemas de carga, Frenos, Suspencion', 1
),
(
    null, 'All Auto', 'Santa Tecla', '6246-5135', 'Neumaticos, Luces, Motor', 1
),
(
    null, 'Auto Experto', 'Ilopango', '8522-1313', 'Bujias, Cambio de filtro, Frenos, Suspencion, Neumaticos', 1
),
(
    null, 'Automotriz Gonzales', 'San Miguel', '1353-5135', 'Cambio de aceite, Motor, Parabrisas, Fugas', 1
),
(
    null, 'Auto-Reparacion', 'Las Arboleras', '6513-6423', 'Transmicion, Neumaticos, Fagas', 1
),
(
    null, 'Auto-Ayudante', 'Apopa', '3588-8358', 'Luces, Cambio de bateria, Frenos', 1
),
(
    null, 'Super Repuestos', 'San Salvador', '3583-2478', 'Cambio de aceite, Frenos, Suspencion, Fugas, Bujias', 1
);

SELECT * FROM Talleres;

--
-- Estructura de tabla para la tabla `Mantenimientos`
--

CREATE TABLE IF NOT EXISTS Mantenimientos
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    idVehiculo INT UNSIGNED NOT NULL,
    idTaller INT UNSIGNED NOT NULL,
    tipoMantenimiento VARCHAR(50) NOT NULL,
    fechaEntrada DATE NOT NULL,
    fechaSalida DATE NOT NULL,
    fallaPrincipal VARCHAR(50) NOT NULL,
    fallaSecundaria VARCHAR(50) NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id),
    KEY (idVehiculo),
    KEY (idTaller),
    CONSTRAINT Mantenimiento_Vehiculo FOREIGN KEY (idVehiculo)
    REFERENCES Vehiculos(id),
    CONSTRAINT Mantenimiento_Taller FOREIGN KEY (idTaller)
    REFERENCES Talleres(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Estructura de tabla para la tabla `Motoristas`
--

CREATE TABLE IF NOT EXISTS Motoristas
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nombres VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    edad INT NOT NULL,
    genero VARCHAR(20) NOT NULL,
    dui VARCHAR(10) UNIQUE NOT NULL,
    licencia VARCHAR(20) UNIQUE NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    telefono VARCHAR(15) NOT NULL,
    observaciones VARCHAR(200) NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT
INTO
	motoristas
VALUES
(
    null, 'Enrique', 'Acevedo Mejia', 30, 'Masculino', '31324265-0', 
    '4444-253573-444-1', 'Av. Madrigal, Usulutan', '7464-2666', 'Sin observaciones.', 1
),
(
    null, 'Jose Israel', 'Alcantar Camacho', 32, 'Masculino', '73275277-0', 
    '4444-254373-444-2', '3 Calle pte. 1-16- Lourdes', '2635-7949', 'Sin observaciones.', 1
),
(
    null, 'Fredy Francisco', 'Aguilar Perez', 25, 'Masculino', '57322666-0', 
    '4444-253673-444-3', 'Col. La Molienda, La Paz', '6732-6827', 'Sin observaciones.', 1
),
(
    null, 'Jose Genaro', 'Lopez Alarcon', 22, 'Masculino', '86257257-0', 
    '4444-278373-444-4', 'Col. Escalon, San Salvador', '2465-4211', 
    'Excede el limite al conducir en carreteras peligrosas', 1
),
(
    null, 'Maria Mireya', 'Manriquez', 29, 'Femenino', '31351325-0', 
    '4444-254373-444-5', 'Col. Santa Teresa. Santa Tecla', '2464-2666', 'Sin observaciones.', 1
),
(
    null, 'Victor Hugo', 'Guerrero Alejo', 35, 'Masculino', '86448369-0', 
    '4444-253763-444-6', 'San Benito, Santa Tecla', '7683-6836', 'Se reporta pasado el tiempo limite', 1
),
(
    null, 'Irma', 'Aguilar Pedrosa', 32, 'Femenino', '16541135-0', 
    '4444-253373-444-7', 'Pasaje 15, Soyapango', '2464-2644', 'Sin observaciones.', 1
),
(
    null, 'Tomas Jose', 'Acosta Canto', 45, 'Masculino', '66663635-0', 
    '4444-253763-444-8', 'Col. La nueva, Santa Ana', '5545-4729', 'Sin observaciones.', 1
),
(
    null, 'Ismael Roberto', 'Martinez Hernandez', 30, 'Masculino', '25768822-0', 
    '4444-253723-444-9', 'Calle Chiltiupan, San Salvador', '7846-2255', 'Sin observaciones.', 1
),
(
    null, 'Antonio Adolfo', 'Navarro Perez', 28, 'Masculino', '86811158-0', 
    '4444-253673-444-0', 'Merliot, Santa Tecla', '6624-2464', 'Los viernes no conduce por la ruta indicada', 1
);

-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `TiposCarga`
--

CREATE TABLE IF NOT EXISTS TiposCarga
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    descripcion VARCHAR(100) UNIQUE NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO TiposCarga VALUES (NULL, 'Productos de vidrio');
INSERT INTO TiposCarga VALUES (NULL, 'Lacteos');

--
-- Estructura de tabla para la tabla `Rutas`
--

CREATE TABLE IF NOT EXISTS Rutas
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    kilometraje DOUBLE NOT NULL,
    origen VARCHAR(100) NOT NULL,
    destino VARCHAR(100) NOT NULL,
    descripcion VARCHAR(200) NOT NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT
INTO
    Rutas
VALUES
(
    null, 53.10,'El Congo, Santa Ana','Aguilares, San Salvador', 'Sin descripcion', 1
),
(
    null, 6.0,'Mejicanos, San Salvador','Apopa, San Salvador', 'Sin descripcion', 1
),
(
    null, 13.0,'San Marcos, San Salvador','Lourdes, La Libertad', 'Sin descripcion', 1
),
(
   null, 13,'Soyapango, San Salvador','Merliot, La Libertad', 'Sin descripcion', 1
),
(
    null, 94.9,'Metapan, Ahuchapan','San Benito, San Salvador', 'Sin descripcion',1
),
(
    null, 68.1,'El Paste, Santa Ana','Cojutepeque, San Salvador', 'Sin descripcion',1
),
(
    null, 197.5,'Santa Tecla, La Libertad','Santa Cruz, La Union', 'Sin descripcion', 1
),
(
    null, 153.1,'Santa Rosa de Lima, Morazan','La Flecha, La Paz', 'Sin descripcion', 1
),
(
    null, 53.4,'Ruta Cañera, Usulutan','Recidencial America, San Miguel', 'Sin descripcion', 1
),
(
    null, 69.0,'San Antonio Los Ranchos, Chalatenango','Amapulapa, San Vicente', 'Sin descripcion', 1
);

--
-- Estructura de tabla para la tabla `Clientes`
--

CREATE TABLE IF NOT EXISTS Clientes
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nombres VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    edad INT NOT NULL,
    genero VARCHAR(20) NOT NULL,
    dui VARCHAR(10) UNIQUE NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    telefono VARCHAR(15) UNIQUE NOT NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


INSERT
INTO
	Clientes
VALUES
(
    NULL, 'William', 'Hernandez', 32, 'Masculino', '12311678-9', 'Santa Ana', '1233-1212', 1
),
(
    null, 'Juan Carlos','Solorzano Perez',30,'Masculino','98765432-1','1a. Calle Oriente, San Vicente','2203-7655',1
),
(
    null, 'Gabriela Daniela','Paz Lopez',25,'Femenino','48546432-1','Col. Las Arboleras, La Libertad','2503-5895',1
),
(
    null, 'Ingrid Yessenia','Moreno Leon',23,'Femenino','12252528-9','Col. Popotlan, San Salvador','2214-65795',1
),
(
   null, 'Daniel Alberto','Figueroa Martinez',36,'Masculino','23465432-1','Av. La Cima, San Salvador','2289-5905',1
),
(
    null, 'William Kevin','Levi Perez',24,'Masculino','88125432-9','Av. Metapan, Ahuachapan','2345-6798',1
),
(
    null, 'Kenia Federica','Mejia Flores',21,'Femenino','64859329-0','Col. Los Girasoles, Chalatenango','2245-0012',1
),
(
    null, 'Daysi Yaneth','Fernandez Cruz',35,'Femenino','23719876-1','Av. Trillisos, SonSonate','7500-2378',1
),
(
    null, 'Manuel Alfonso','Flores Grande',20,'Masculino','98732132-1','Col. Cojutepeque, San Salvador','2213-5491',1
),
(
    null, 'Rene Alejandro','Hernandez Romero',30,'Masculino','54782190-6','Col. Ponderosa, San Miguel','7789-0021',1
),
(
    null, 'Wendy Alexandra','Moreno America',45,'Femenina','65783091-1','Av. Las Montañas, Usulutan','2203-1273',1
);


SELECT * FROM Clientes;

--
-- Estructura de tabla para la tabla `Viajes`
--

CREATE TABLE IF NOT EXISTS Viajes
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    idRuta INT UNSIGNED NOT NULL,
    idMotorista INT UNSIGNED NOT NULL,
    idVehiculo INT UNSIGNED NOT NULL,
    idTipoCarga INT UNSIGNED NOT NULL,
    idCliente INT UNSIGNED NOT NULL,
    fechaSalida DATE NOT NULL,
    fechaEntrada DATE NOT NULL,
    cantidadCarga DOUBLE NOT NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id),
    KEY (idRuta),
    KEY (idMotorista),
    KEY (idVehiculo),
    CONSTRAINT Viaje_Ruta FOREIGN KEY (idRuta)
    REFERENCES Rutas(id),
    CONSTRAINT Viaje_Motorista FOREIGN KEY (idMotorista)
    REFERENCES Motoristas(id),
    CONSTRAINT Viaje_Vehiculo FOREIGN KEY (idVehiculo)
    REFERENCES Vehiculos(id),
    CONSTRAINT Viaje_Cliente FOREIGN KEY (idCliente)
    REFERENCES Clientes(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT
INTO
	Viajes
VALUES
(
    null, 1, 1, 1, 1, 1, '2018/10/21', '2018/10/21', 150.3, 1
),
(
    null, 2, 2, 2, 1, 2, '2018/10/21', '2018/10/21', 253.5, 1
),
(
    null, 3, 3, 3, 1, 3, '2018/10/21', '2018/10/21', 500.0, 1
),
(
   null, 4, 4, 4, 1, 4, '2018/10/21', '2018/10/21', 299.56, 1
);

--
-- Estructura de tabla para la tabla `TiposUsuario`
--

CREATE TABLE IF NOT EXISTS TiposUsuario
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    descripcion VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO TiposUsuario VALUES (NULL, 'Administrador');
INSERT INTO TiposUsuario VALUES (NULL, 'Reporteria');

-- == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `Usuarios`
--

CREATE TABLE IF NOT EXISTS Usuarios
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    idTipoUsuario INT UNSIGNED NOT NULL,
    nombre VARCHAR(50) UNIQUE NOT NULL,
    pass VARCHAR(200) NOT NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id),
    KEY (idTipoUsuario),
    CONSTRAINT Usuario_TU FOREIGN KEY (idTipoUsuario)
    REFERENCES TiposUsuario(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO Usuarios VALUES (NULL, 1, 'Admin', '123', 1);
INSERT INTO Usuarios VALUES (NULL, 2, 'Report', '123', 1);


-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
--
-- Procedimientos para la tabla `Usuarios`
--
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==

-- Verifica que el usuario exista
DELIMITER $$
CREATE PROCEDURE auth(IN nom VARCHAR(50))
BEGIN
	-- Acción a realizar
	SELECT * FROM usuarios WHERE nombre = nom AND estado != 0 LIMIT 1;
END
$$

-- Verifica que el usuario y la contraseña coincidan
DELIMITER $$
CREATE PROCEDURE verify(IN nom VARCHAR(50), IN pwd VARCHAR(200))
BEGIN
	-- Acción a realizar
	SELECT idTipoUsuario FROM usuarios WHERE nombre = nom
    AND pass = pwd AND estado != 0 LIMIT 1;
END
$$

-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
--
-- Procedimientos para la tabla `Motoristas`
--
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==

-- Muestra todos los motoristas con estado 1
DELIMITER $$
CREATE PROCEDURE showMot()
BEGIN
	-- Acción a realizar
	SELECT * FROM Motoristas WHERE estado != 0 ORDER BY id DESC;
END
$$

-- Muestra todos los motoristas que coincidan con la busqueda
/*DELIMITER $$
CREATE PROCEDURE findMot(IN val VARCHAR(50))
BEGIN
	-- Acción a realizar
	SELECT * FROM Motoristas WHERE estado != 0
    AND (dui LIKE ('%' + val + '%') OR apellidos LIKE ('%' + val + '%') OR nombres LIKE ('%' + val + '%'))
    ORDER BY id DESC;
END
$$*/

-- Agrega un nuevo motorista
DELIMITER $$
CREATE PROCEDURE addMot(IN nom VARCHAR(50), IN ape VARCHAR(50), IN ed INT,
		IN gen VARCHAR(20), IN _dui VARCHAR(10), IN lic VARCHAR(20), 
        IN dir VARCHAR(100), IN tel VARCHAR(15), IN obs VARCHAR(200))
BEGIN
	IF NOT EXISTS (SELECT * FROM Motoristas WHERE dui = _dui)
	THEN
		-- Acción a realizar
		INSERT INTO motoristas VALUES (NULL, nom, ape, ed, gen, _dui, lic, dir, tel, obs, 1);
	END IF;
END
$$

-- Edita un motorista
DELIMITER $$
CREATE PROCEDURE editMot(IN nom VARCHAR(50), IN ape VARCHAR(50), IN ed INT, IN gen VARCHAR(20), 
		IN _dui VARCHAR(10), IN lic VARCHAR(20), IN dir VARCHAR(100), IN tel VARCHAR(15), 
        IN obs VARCHAR(200), IN _id INT)
BEGIN
		-- Acción a realizar
		UPDATE motoristas SET nombres = nom, apellidos = ape, edad = ed, genero = gen,
		dui = _dui, licencia = lic, direccion = dir, telefono = tel, observaciones = obs
		WHERE id = _id;
END
$$

-- Actualiza el estado del motorista a '0'
DELIMITER $$
CREATE PROCEDURE deleteMot(IN _id INT)
BEGIN
	-- Acción a realizar
	UPDATE motoristas SET estado = 0 WHERE id = _id;
END
$$

-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
--
-- Procedimientos para la tabla `Vehiculos`
--
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==
-- == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == ==

-- Muestra todos los vehiculos activos
DELIMITER $$
CREATE PROCEDURE showVeh()
BEGIN
	-- Acción a realizar
	SELECT * FROM Vehiculos WHERE estado != 0 ORDER BY id DESC;
END
$$

-- Muestra todos los vehiculos que coincidan con la busqueda
/*DELIMITER $$
CREATE PROCEDURE findVeh(IN val VARCHAR(50))
BEGIN
	-- Acción a realizar
	SELECT * FROM Vehiculos WHERE estado != 0 
    AND (placa LIKE ('%' + val + '%') OR modelo LIKE ('%' + val + '%') 
    OR marca LIKE ('%' + val + '%')) ORDER BY id DESC;
END
$$*/

-- Agrega un nuevo vehiculo
DELIMITER $$
CREATE PROCEDURE addVeh(IN fechaA VARCHAR(20), IN tal BOOLEAN, IN model VARCHAR(50),
		IN pla VARCHAR(10), IN mar VARCHAR(50), IN kmRec DOUBLE, IN fechaUlCaAc VARCHAR(20),
		IN fechaUlMan VARCHAR(20), IN obs VARCHAR(200))
BEGIN
	IF NOT EXISTS (SELECT * FROM Vehiculos WHERE placa = pla)
    THEN
		-- Acción a realizar
		INSERT INTO Vehiculos VALUES(null, fechaA, tal, model, pla, mar, 
		kmRec, fechaUlCaAc, fechaUlMan, obs, 1);
    END IF;
END
$$

-- Edita un vehiculo
DELIMITER $$
CREATE PROCEDURE editVeh(IN fechaA VARCHAR(20), IN tal BOOLEAN, IN model VARCHAR(50),
		IN pla VARCHAR(10), IN mar VARCHAR(50), IN kmRec DOUBLE, IN fechaUlCaAc VARCHAR(20),
		IN fechaUlMan VARCHAR(20), IN obs VARCHAR(200), IN _id INT)
BEGIN
		-- Acción a realizar
		UPDATE Vehiculos SET fechaAdquisicion = fechaA, taller = tal, modelo = model, placa = pla,
		marca = mar, kilometrosrecorridos = kmRec, fechaUltimoCambioAceite = fechaUlCaAc, 
		fechaUltimoMantenimiento = fechaUlMan, observaciones = obs WHERE id = _id;
END
$$

-- Actualiza el estado del vehiculo a '0'
DELIMITER $$
CREATE PROCEDURE deleteVeh(IN _id INT)
BEGIN
	-- Acción a realizar
	UPDATE Vehiculos SET estado = 0 WHERE id = _id;
END
$$

-- Muestra todos los Mantenimientos activos
DELIMITER $$
CREATE PROCEDURE showMant()
BEGIN
	-- Acción a realizar
	SELECT m.*, v.placa, t.nombre FROM Mantenimientos m INNER JOIN Vehiculos v 
    ON m.idVehiculo = v.id INNER JOIN Talleres t ON m.idTaller = t.id WHERE m.estado != 0 ORDER BY id DESC;
END
$$

DELIMITER $$
CREATE PROCEDURE showVia()
BEGIN
	-- Acción a realizar
	SELECT 
    v.*, CONCAT(r.origen, ' - ', r.destino) AS test, mot.dui, mot.nombres, veh.placa, tc.descripcion, c.nombres
  FROM 
    Viajes v 
  INNER JOIN 
    Rutas r 
  ON v.idRuta = r.id 
  INNER JOIN 
    Motoristas mot 
  ON 
    v.idMotorista = mot.id 
  INNER JOIN 
    Vehiculos veh 
  ON 
    v.idVehiculo = veh.id
  INNER JOIN 
    TiposCarga tc 
  ON 
    v.idTipoCarga = tc.id
  INNER JOIN 
    Clientes c 
  ON 
    v.idCliente = c.id
  WHERE v.estado != 0 ORDER BY id DESC;
END
$$

INSERT
INTO
    Mantenimientos
VALUES
(null, 1, 1, 'Cambio de llantas', '2018-09-22', '2018-10-1', 'Motor', 'Filtros', 1),
(null, 3, 1, 'Cambio de aceite', '2018-09-07', '2018-09-10', 'Motor', 'Tranmision', 1),
(null, 4, 1, 'Cambio de puertas', '2018-09-04', '2018-09-06', 'Sistema de carga', 'Bujias', 1),
(null, 5, 1, 'Limpieza', '2018-09-02', '2018-09-03', 'Amortiguadores', 'Filtros', 1);

SELECT * FROM Talleres WHERE estado != 0 ORDER BY id DESC;