package com.dao;

import com.connection.Conexion;
import com.models.Usuario;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: DaoUsuario
 * Version: 2.0
 * Fecha: 10/08/2018
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class DaoUsuario extends Conexion
{
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public Boolean auth(String user)
    {
        Boolean estado = false;
        ResultSet res = null;
        try
        {
            conectar();
            String sql = "{CALL auth(?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, user);
            res = pre.executeQuery();
            if (res.next())
            {                
                estado = true;
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al autenticar:\n" 
                    + e.toString(), "Error", 0);
        }
        return estado;
    }
    
    public Integer verify(String user, String pass)
    {
        Integer id = -1;
        ResultSet res = null;
        try
        {
            String sql = "{CALL verify(?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, user.replace("'", "''").replace("\"", "''"));
            pre.setString(2, pass.replace("'", "''").replace("\"", "''"));
            res = pre.executeQuery();
            if (res.next())
            {
                id = res.getInt(1);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al verificar:\n" 
                    + e.toString(), "Error", 0);
        }
        return id;
    }
    //</editor-fold>
}
