package com.dao;

import com.connection.Conexion;
import com.models.Vehiculo;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.*;

/**
 * Nombre de la clase: DaoVehiculo
 * Version: 2.0
 * Fecha: 10/08/2018
 * Copyright: ITCA-FEPADE
 * @author Brayan Mejia, Verónica Maribel, Juan Pablo Elias
 */
public class DaoVehiculo extends Conexion
{
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public List all()
    {
        List vehiculos = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "{CALL showVeh()}";
            PreparedStatement pre = getConn().prepareCall(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Vehiculo ve = new Vehiculo();
                ve.setId(res.getInt(1));
                ve.setFechaAdquisicion(res.getString(2));
                ve.setTaller(res.getBoolean(3));
                ve.setModelo(res.getString(4));
                ve.setPlaca(res.getString(5));
                ve.setMarca(res.getString(6));
                ve.setKilometrosRecorridos(res.getDouble(7));
                ve.setFechaUltimoCambioAceite(res.getString(8));
                ve.setFechaUltimoMantenimiento(res.getString(9));
                ve.setObservaciones(res.getString(10));
                ve.setEstado(res.getInt(11));
                vehiculos.add(ve);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return vehiculos;
    }
    
    public List find(String val)
    {
        List vehiculos = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "SELECT * FROM Vehiculos WHERE estado != 0 "
                    + "AND (placa LIKE '%" + val + "%' OR modelo LIKE '%" + val + "%' "
                    + "OR marca LIKE '%" + val + "%') ORDER BY id DESC;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Vehiculo ve = new Vehiculo();
                ve.setId(res.getInt(1));
                ve.setFechaAdquisicion(res.getString(2));
                ve.setTaller(res.getBoolean(3));
                ve.setModelo(res.getString(4));
                ve.setPlaca(res.getString(5));
                ve.setMarca(res.getString(6));
                ve.setKilometrosRecorridos(res.getDouble(7));
                ve.setFechaUltimoCambioAceite(res.getString(8));
                ve.setFechaUltimoMantenimiento(res.getString(9));
                ve.setObservaciones(res.getString(10));
                ve.setEstado(res.getInt(11));
                vehiculos.add(ve);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return vehiculos;
    }
    
    public int add(Vehiculo v)
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL addVeh(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, v.getFechaAdquisicion());
            pre.setBoolean(2, v.getTaller());
            pre.setString(3, v.getModelo());
            pre.setString(4, v.getPlaca());
            pre.setString(5, v.getMarca());
            pre.setDouble(6, v.getKilometrosRecorridos());
            pre.setString(7, v.getFechaUltimoCambioAceite());
            pre.setString(8, v.getFechaUltimoMantenimiento());
            pre.setString(9, v.getObservaciones());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al agregar el registro:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int edit(Vehiculo v)
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL editVeh(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, v.getFechaAdquisicion());
            pre.setBoolean(2, v.getTaller());
            pre.setString(3, v.getModelo());
            pre.setString(4, v.getPlaca());
            pre.setString(5, v.getMarca());
            pre.setDouble(6, v.getKilometrosRecorridos());
            pre.setString(7, v.getFechaUltimoCambioAceite());
            pre.setString(8, v.getFechaUltimoMantenimiento());
            pre.setString(9, v.getObservaciones());
            pre.setInt(10, v.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al editar el registro:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int delete(Vehiculo v)
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL deleteVeh(?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setInt(1, v.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al eliminar el registro:\n" 
                    + e.toString(), "Error", 0);
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public void print()
    {
        JasperReport report;
        try
        {
            conectar();
            report = JasperCompileManager.compileReport("src/com/reports/RptVehiculos.jrxml");
            JasperPrint print = JasperFillManager.fillReport(report, null, getConn());
            JasperViewer.viewReport(print, false);
        }
        catch (JRException e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar el reporte:\n"
                    + e.toString(), "Error", 0);
        }
    }
    
    public void printOne(Integer id)
    {
        JasperReport report;
        try
        {
            conectar();
            Map params = new HashMap();
            params.put("idV", id);
            report = JasperCompileManager.compileReport("src/com/reports/RptVehiculo.jrxml");
            JasperPrint print = JasperFillManager.fillReport(report, params, getConn());
            JasperViewer.viewReport(print, false);
        }
        catch (JRException e)
        {
            JOptionPane.showMessageDialog(null, "Error al mostrar el reporte:\n"
                    + e.toString(), "Error", 0);
        }
    }
    //</editor-fold>
}
