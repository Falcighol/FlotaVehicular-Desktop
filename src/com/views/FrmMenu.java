package com.views;

import com.dao.DaoMotorista;
import com.dao.DaoVehiculo;
import com.models.Usuario;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: FrmMenu
 * Version: 1.0
 * Fecha: 18/8/2018
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class FrmMenu extends javax.swing.JFrame
{
    Usuario u = new Usuario();
    FrmLogin log = new FrmLogin();
    DaoMotorista daoM = new DaoMotorista();
    DaoVehiculo daoV = new DaoVehiculo();
    Boolean motActive = true;
    Boolean vehActive = false;
    
    PnlMotorista pMot = null;
    PnlVehiculos pVeh = null;
    
    public FrmMenu(Integer idTu, String user)
    {
        initComponents();
        setLocationRelativeTo(this);
        u.setIdTipo(idTu);
        u.setNombre(user);
        jLblBienvenida.setText("Bienvenido/a, " + u.getNombre());
        motoristas();
        setIconImage(new ImageIcon(getClass().getResource("/com/images/icon.png")).getImage());
    }

    private FrmMenu()
    {
        
    }
    
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public void motoristas()
    {
        // Active color
        jPnlMotoristas.setBackground(new Color(69,77,84));
        // Desactive color
        jPnlVehiculos.setBackground(new Color(59,66,72));
        // Reseteando componente
        pVeh = null;
        // Instancia y llamado a panel de motoristas
        pMot = new PnlMotorista(u.getIdTipo(), u.getNombre());
        pMot.setSize(820, 530);
        pMot.setLocation(0, 0);
        jPnlDash.removeAll();
        jPnlDash.add(pMot, BorderLayout.CENTER);
        jPnlDash.revalidate();
        jPnlDash.repaint();
        motActive = true;
        vehActive = false;
    }
    
    public void vehiculos()
    {
        // Active color
        jPnlVehiculos.setBackground(new Color(69,77,84));
        // Desactive color
        jPnlMotoristas.setBackground(new Color(59,66,72));
        // Reseteando componente
        pMot = null;
        // Instancia y llamado a panel de motoristas
        pVeh = new PnlVehiculos(u.getIdTipo(), u.getNombre());
        pVeh.setSize(820, 530);
        pVeh.setLocation(0, 0);
        jPnlDash.removeAll();
        jPnlDash.add(pVeh, BorderLayout.CENTER);
        jPnlDash.revalidate();
        jPnlDash.repaint();
        motActive = false;
        vehActive = true;
    }
    
    public void loadMotoristas()
    {
        if (vehActive)
        {
            if (pVeh.adding || pVeh.editing)
            {
                if (JOptionPane.showConfirmDialog(this, 
                        "Todos los cambios no guardados se perderán.\n"
                        + "¿Seguro que desea continuar?", "Ir a Motoristas", 
                        JOptionPane.YES_NO_OPTION) == 0)
                {
                    if (pMot == null)
                    {
                        motoristas();
                    }
                }
            }
            else
            {
                if (pMot == null)
                {
                    motoristas();
                }
            }
        }
    }
    
    public void loadVehiculos()
    {
        if (motActive)
        {
            if (pMot.adding || pMot.editing)
            {
                if (JOptionPane.showConfirmDialog(this,
                        "Todos los cambios no guardados se perderán.\n"
                        + "¿Seguro que desea continuar?", "Ir a Vehiculos", 
                        JOptionPane.YES_NO_OPTION) == 0)
                {
                    if (pVeh == null)
                    {
                        vehiculos();
                    }
                }
            }
            else
            {
                if (pVeh == null)
                {
                    vehiculos();
                }
            }
        }
    }
    //</editor-fold>
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlMain = new javax.swing.JPanel();
        jLblLogout = new javax.swing.JLabel();
        jPnlLeft = new javax.swing.JPanel();
        jLblLeftTitle = new javax.swing.JLabel();
        jPnlMotoristas = new javax.swing.JPanel();
        jLblMotoristas = new javax.swing.JLabel();
        jPnlVehiculos = new javax.swing.JPanel();
        jLblVehiculos = new javax.swing.JLabel();
        jPnlRptMot = new javax.swing.JPanel();
        jLbRptMot = new javax.swing.JLabel();
        jPnlRptVeh = new javax.swing.JPanel();
        jLblRptVeh = new javax.swing.JLabel();
        jPnlLeftDown = new javax.swing.JPanel();
        jSptUser = new javax.swing.JSeparator();
        jLblBienvenida = new javax.swing.JLabel();
        jPnlDash = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Inicio");
        setName("Dashboard"); // NOI18N
        setResizable(false);

        jPnlMain.setBackground(new java.awt.Color(254, 254, 254));
        jPnlMain.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblLogout.setFont(new java.awt.Font("Montserrat", 0, 14)); // NOI18N
        jLblLogout.setForeground(new java.awt.Color(60, 63, 65));
        jLblLogout.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Exit_25px.png"))); // NOI18N
        jLblLogout.setText("Cerrar sesión");
        jLblLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLblLogout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblLogoutMouseClicked(evt);
            }
        });
        jPnlMain.add(jLblLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 30, 130, 30));

        jPnlLeft.setBackground(new java.awt.Color(52, 58, 64));
        jPnlLeft.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblLeftTitle.setFont(new java.awt.Font("Montserrat", 0, 28)); // NOI18N
        jLblLeftTitle.setForeground(new java.awt.Color(255, 255, 255));
        jLblLeftTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblLeftTitle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Home_30px.png"))); // NOI18N
        jLblLeftTitle.setText(" INICIO");
        jPnlLeft.add(jLblLeftTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 220, 50));

        jPnlMotoristas.setBackground(new java.awt.Color(69, 77, 84));
        jPnlMotoristas.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblMotoristas.setFont(new java.awt.Font("Segoe UI Semilight", 0, 18)); // NOI18N
        jLblMotoristas.setForeground(new java.awt.Color(255, 255, 255));
        jLblMotoristas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblMotoristas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/User Account_25px.png"))); // NOI18N
        jLblMotoristas.setText("  Motoristas");
        jLblMotoristas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblMotoristasMouseClicked(evt);
            }
        });
        jPnlMotoristas.add(jLblMotoristas, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 60));

        jPnlLeft.add(jPnlMotoristas, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 240, 60));

        jPnlVehiculos.setBackground(new java.awt.Color(59, 66, 72));
        jPnlVehiculos.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblVehiculos.setFont(new java.awt.Font("Segoe UI Semilight", 0, 18)); // NOI18N
        jLblVehiculos.setForeground(new java.awt.Color(255, 255, 255));
        jLblVehiculos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblVehiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Truck_25px.png"))); // NOI18N
        jLblVehiculos.setText("  Vehiculos");
        jLblVehiculos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblVehiculosMouseClicked(evt);
            }
        });
        jPnlVehiculos.add(jLblVehiculos, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 60));

        jPnlLeft.add(jPnlVehiculos, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 170, 240, 60));

        jPnlRptMot.setBackground(new java.awt.Color(59, 66, 72));
        jPnlRptMot.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLbRptMot.setFont(new java.awt.Font("Segoe UI Semilight", 0, 18)); // NOI18N
        jLbRptMot.setForeground(new java.awt.Color(255, 255, 255));
        jLbRptMot.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLbRptMot.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Graph Report_25px.png"))); // NOI18N
        jLbRptMot.setText("Rpt. Motoristas");
        jLbRptMot.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLbRptMotMouseClicked(evt);
            }
        });
        jPnlRptMot.add(jLbRptMot, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 60));

        jPnlLeft.add(jPnlRptMot, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 240, 60));

        jPnlRptVeh.setBackground(new java.awt.Color(59, 66, 72));
        jPnlRptVeh.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblRptVeh.setFont(new java.awt.Font("Segoe UI Semilight", 0, 18)); // NOI18N
        jLblRptVeh.setForeground(new java.awt.Color(255, 255, 255));
        jLblRptVeh.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblRptVeh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Graph Report_25px.png"))); // NOI18N
        jLblRptVeh.setText("Rpt. Vehículos");
        jLblRptVeh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLblRptVehMouseClicked(evt);
            }
        });
        jPnlRptVeh.add(jLblRptVeh, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 60));

        jPnlLeft.add(jPnlRptVeh, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 240, 60));

        jPnlLeftDown.setBackground(new java.awt.Color(52, 58, 64));
        jPnlLeftDown.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPnlLeft.add(jPnlLeftDown, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 240, 280));

        jPnlMain.add(jPnlLeft, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 630));

        jSptUser.setBackground(new java.awt.Color(254, 254, 254));
        jSptUser.setForeground(new java.awt.Color(69, 77, 84));
        jPnlMain.add(jSptUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 80, 800, 10));

        jLblBienvenida.setFont(new java.awt.Font("Montserrat", 0, 26)); // NOI18N
        jLblBienvenida.setForeground(new java.awt.Color(69, 77, 84));
        jLblBienvenida.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLblBienvenida.setText("Bienvenido/a,  ");
        jPnlMain.add(jLblBienvenida, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 20, 550, 40));

        jPnlDash.setBackground(new java.awt.Color(254, 254, 254));

        javax.swing.GroupLayout jPnlDashLayout = new javax.swing.GroupLayout(jPnlDash);
        jPnlDash.setLayout(jPnlDashLayout);
        jPnlDashLayout.setHorizontalGroup(
            jPnlDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 820, Short.MAX_VALUE)
        );
        jPnlDashLayout.setVerticalGroup(
            jPnlDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 530, Short.MAX_VALUE)
        );

        jPnlMain.add(jPnlDash, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 90, 820, 530));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, 1081, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //<editor-fold defaultstate="collapsed" desc="Eventos de controles">
    private void jLblMotoristasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblMotoristasMouseClicked
        loadMotoristas();
    }//GEN-LAST:event_jLblMotoristasMouseClicked

    private void jLblVehiculosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblVehiculosMouseClicked
        loadVehiculos();
    }//GEN-LAST:event_jLblVehiculosMouseClicked

    private void jLblLogoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblLogoutMouseClicked
        if (JOptionPane.showConfirmDialog(this, "¿Seguro que desea cerrar la sesión?", 
                "Cerrar sesión", JOptionPane.YES_NO_OPTION) == 0)
        {
            log.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_jLblLogoutMouseClicked

    private void jLbRptMotMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLbRptMotMouseClicked
        if (JOptionPane.showConfirmDialog(this, "¿Desea mostrar el reporte de motoristas?", 
                "Imprimir", JOptionPane.YES_NO_OPTION) == 0)
        {
            daoM.print();
        }
    }//GEN-LAST:event_jLbRptMotMouseClicked

    private void jLblRptVehMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLblRptVehMouseClicked
        if (JOptionPane.showConfirmDialog(this, "¿Desea mostrar el reporte de vehículos?", 
                "Imprimir", JOptionPane.YES_NO_OPTION) == 0)
        {
            daoV.print();
        }
    }//GEN-LAST:event_jLblRptVehMouseClicked
    //</editor-fold>
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLbRptMot;
    private javax.swing.JLabel jLblBienvenida;
    private javax.swing.JLabel jLblLeftTitle;
    private javax.swing.JLabel jLblLogout;
    private javax.swing.JLabel jLblMotoristas;
    private javax.swing.JLabel jLblRptVeh;
    private javax.swing.JLabel jLblVehiculos;
    public static javax.swing.JPanel jPnlDash;
    private javax.swing.JPanel jPnlLeft;
    private javax.swing.JPanel jPnlLeftDown;
    public javax.swing.JPanel jPnlMain;
    private javax.swing.JPanel jPnlMotoristas;
    private javax.swing.JPanel jPnlRptMot;
    private javax.swing.JPanel jPnlRptVeh;
    private javax.swing.JPanel jPnlVehiculos;
    private javax.swing.JSeparator jSptUser;
    // End of variables declaration//GEN-END:variables
}
