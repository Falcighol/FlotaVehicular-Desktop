package com.views;

import com.dao.DaoMotorista;
import com.models.Motorista;
import com.models.Usuario;
import com.utils.Validations;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: PnlMotorista
 * Version: 1.0
 * Fecha: 18/8/2018
 * Copyright: ITCA-FEPADE
 * @author Kevin Lovos, Álvaro Pérez, Juan Pablo Elias
 */
public class PnlMotorista extends javax.swing.JPanel
{
    public Boolean adding = false;
    public Boolean editing = false;
    Usuario u = new Usuario();
    Validations val = new Validations();
    DaoMotorista daoM = new DaoMotorista();
    Motorista mot = new Motorista();
    
    public PnlMotorista(Integer idTu, String user)
    {
        initComponents();
        controls(false);
        all();
        buttons();
        u.setIdTipo(idTu);
        u.setNombre(user);
        jTxtId.setVisible(false);
        adding = false;
        editing = false;
        if (idTu != 1)
        {
            jBtnAdd.setEnabled(false);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(false);
            jBtnCancel.setEnabled(false);
            jBtnPrint.setEnabled(true);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public Boolean existRows()
    {
        Boolean estado = false;
        if (jTable.getRowCount() > 0)
        {
            estado = true;
        }
        return estado;
    }
    
    public void find(String val)
    {
        String[] columnas = {"ID", "Nombres", "Apellidos", "Edad", "DUI", "Telefono", 
                "Dirección", "Licencia", "Genero","Observaciones"};
        Object[] obj = new Object[10];
        DefaultTableModel tabla = new DefaultTableModel(null, columnas);
        List ls;
        try
        {
            ls = daoM.find(val);
            for (int i = 0; i < ls.size(); i++)
            {
                mot = (Motorista) ls.get(i);
                obj[0] = mot.getId();
                obj[1] = mot.getNombres();
                obj[2] = mot.getApellidos();
                obj[3] = mot.getEdad();
                obj[4] = mot.getDui();
                obj[5] = mot.getTelefono();
                obj[6] = mot.getDireccion();
                obj[7] = mot.getLicencia();
                obj[8] = mot.getGenero();
                obj[9] = mot.getObsevaciones();
                tabla.addRow(obj);
            }
            this.jTable.setModel(tabla);
            hideColums();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error generando tabla: " + e.toString(), 
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void buttons()
    {
        if (!existRows())
        {
            jBtnAdd.setEnabled(true);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(false);
            jBtnCancel.setEnabled(false);
            jBtnPrint.setEnabled(false);
        }
        else
        {
            jBtnAdd.setEnabled(true);
            jBtnEdit.setEnabled(true);
            jBtnDelete.setEnabled(true);
            jBtnSave.setEnabled(false);
            jBtnCancel.setEnabled(false);
            jBtnPrint.setEnabled(true);
        }
        if (adding || editing)
        {
            jBtnAdd.setEnabled(false);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(true);
            jBtnCancel.setEnabled(true);
            jBtnPrint.setEnabled(false);
        }
    }
    
    public void controls(Boolean option)
    {
        jTxtNombres.setEnabled(option);
        jTxtApellidos.setEnabled(option);
        jSpnEdad.setEnabled(option);
        jTxtDui.setEnabled(option);
        jTxtTelefono.setEnabled(option);
        jTxtDireccion.setEnabled(option);
        jTxtLicencia.setEnabled(option);
        jTxtObservaciones.setEnabled(option);
        jRdbM.setEnabled(option);
        jRdbF.setEnabled(option);
        jSptNombres.setEnabled(option);
        jSptApellidos.setEnabled(option);
        jSptEdad.setEnabled(option);
        jSptDui.setEnabled(option);
        jSptTelefono.setEnabled(option);
        jSptDireccion.setEnabled(option);
        jSptLicencia.setEnabled(option);
        jSptObservaciones.setEnabled(option);
    }
    
    public void all()
    {
        String[] columnas = {"ID", "Nombres", "Apellidos", "Edad", "DUI", "Telefono", 
                "Dirección", "Licencia", "Genero", "Observaciones"};
        Object[] obj = new Object[10];
        DefaultTableModel tabla = new DefaultTableModel(null, columnas);
        List ls;
        try
        {
            ls = daoM.all();
            for (int i = 0; i < ls.size(); i++)
            {
                mot = (Motorista) ls.get(i);
                obj[0] = mot.getId();
                obj[1] = mot.getNombres();
                obj[2] = mot.getApellidos();
                obj[3] = mot.getEdad();
                obj[4] = mot.getDui();
                obj[5] = mot.getTelefono();
                obj[6] = mot.getDireccion();
                obj[7] = mot.getLicencia();
                obj[8] = mot.getGenero();
                obj[9] = mot.getObsevaciones();
                tabla.addRow(obj);
            }
            this.jTable.setModel(tabla);
            hideColums();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error generando tabla: " + e.toString(), 
                    "Error" + e.getMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void hideColums()
    {
        jTable.getColumnModel().getColumn(0).setMaxWidth(0);
        jTable.getColumnModel().getColumn(0).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        jTable.getColumnModel().getColumn(5).setMaxWidth(0);
        jTable.getColumnModel().getColumn(5).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(5).setMinWidth(0);
        jTable.getColumnModel().getColumn(6).setMaxWidth(0);
        jTable.getColumnModel().getColumn(6).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);
        jTable.getColumnModel().getColumn(7).setMaxWidth(0);
        jTable.getColumnModel().getColumn(7).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(7).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(7).setMinWidth(0);
        jTable.getColumnModel().getColumn(8).setMaxWidth(0);
        jTable.getColumnModel().getColumn(8).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(8).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(8).setMinWidth(0);
        jTable.getColumnModel().getColumn(9).setMaxWidth(0);
        jTable.getColumnModel().getColumn(9).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(9).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(9).setMinWidth(0);
    }
    
    public void add()
    {
        if (!adding && !editing && u.getIdTipo() == 1)
        {
            adding = true;
            buttons();
            controls(true);
            clean();
            jTable.setEnabled(false);
            jTxtObservaciones.setText("Sin observaciones.");
        }
    }
    
    public void edit()
    {
        if (!adding && !editing && u.getIdTipo() == 1)
        {
            if (validacion())
            {
                if (JOptionPane.showConfirmDialog(this, "¿Desea editar el registro?",
                        "Mensaje", JOptionPane.YES_NO_OPTION) == 0)
                {
                    editing = true;
                    buttons();
                    controls(true);
                    jTable.setEnabled(false);
                }
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Seleccione un registro para editar.",
                        "", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public void delete()
    {
        try
        {
            if (!adding && !editing && u.getIdTipo() == 1)
            {
                if (validacion())
                {
                    if (JOptionPane.showConfirmDialog(this, "¿Desea eliminar el registro?",
                            "Mensaje", JOptionPane.YES_NO_OPTION) == 0)
                    {
                        mot.setId(Integer.parseInt(jTxtId.getText()));
                        daoM.delete(mot);
                        all();
                        clean();
                        JOptionPane.showMessageDialog(this, "Registro eliminado exitosamente.",
                            "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Seleccione un registro para eliminar.",
                            "Mensaje", JOptionPane.WARNING_MESSAGE);
                }
                buttons();
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, "Error al eliminar: " + e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
            buttons();
        }
    }
    
    public void save()
    {
        try
        {
            if (adding || editing)
            {
                if (validacion())
                {
                    if (adding && !editing)
                    {
                        mot.setId(0);
                        mot.setNombres(jTxtNombres.getText());
                        mot.setApellidos(jTxtApellidos.getText());
                        mot.setEdad(Integer.parseInt(jSpnEdad.getValue().toString()));
                        mot.setDui(jTxtDui.getText());
                        mot.setTelefono(jTxtTelefono.getText());
                        mot.setDireccion(jTxtDireccion.getText());
                        mot.setLicencia(jTxtLicencia.getText());
                        mot.setObsevaciones(jTxtObservaciones.getText());
                        if (jRdbM.isSelected())
                            mot.setGenero("Masculino");
                        else if (jRdbF.isSelected())
                            mot.setGenero("Femenino");
                        daoM.add(mot);
                    }
                    else if (!adding && editing)
                    {
                        mot.setId(Integer.parseInt(jTxtId.getText()));
                        mot.setNombres(jTxtNombres.getText());
                        mot.setApellidos(jTxtApellidos.getText());
                        mot.setEdad(Integer.parseInt(jSpnEdad.getValue().toString()));
                        mot.setDui(jTxtDui.getText());
                        mot.setTelefono(jTxtTelefono.getText());
                        mot.setDireccion(jTxtDireccion.getText());
                        mot.setLicencia(jTxtLicencia.getText());
                        mot.setObsevaciones(jTxtObservaciones.getText());
                        if (jRdbM.isSelected())
                            mot.setGenero("Masculino");
                        else if (jRdbF.isSelected())
                            mot.setGenero("Femenino");
                        daoM.edit(mot);
                    }
                    adding = false;
                    editing = false;
                    controls(false);
                    jTable.setEnabled(true);
                    clean();
                    all();
                    JOptionPane.showMessageDialog(this, "Registro guardado exitosamente.",
                                "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    buttons();
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Ingrese los datos obligatorios.",
                            "Mensaje", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, "Error al guardar: " + e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
            buttons();
        }
    }
    
    public void cancel()
    {
        if (adding || editing)
        {
            adding = false;
            editing = false;
            controls(false);
            buttons();
            jTable.setEnabled(true);
            clean();
            all();
        }
    }
    
    public void clean()
    {
        jTxtId.setText("");
        jTxtNombres.setText("");
        jTxtApellidos.setText("");
        jSpnEdad.setValue(18);
        jTxtDui.setText("");
        jTxtTelefono.setText("");
        jTxtDireccion.setText("");
        jTxtLicencia.setText("");
        jTxtObservaciones.setText("");
        jRdbM.setSelected(true);
        jRdbF.setSelected(false);
    }
    
    public void print()
    {
        if (!adding && !editing)
        {
            if (validacion())
            {
                if (JOptionPane.showConfirmDialog(this, "¿Desea mostrar el reporte del motorista "
                        + jTxtNombres.getText() + "?", 
                    "Imprimir", JOptionPane.YES_NO_OPTION) == 0)
                {
                    daoM.printOne(Integer.parseInt(jTxtId.getText()));
                }
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Seleccione un registro para imprimir.",
                        "", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public Boolean validacion()
    {
        Boolean estado = true;
        if (val.IsNullOrEmpty(jTxtNombres.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtApellidos.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtDui.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtTelefono.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtDireccion.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtLicencia.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtObservaciones.getText()))
        {
            estado = false;
        }
        return estado;
    }
    
    public void llenarCampos()
    {
        if (existRows() && !adding && !editing)
        {
            if (this.jTable.getSelectedRow() > -1)
            {
                int fila = this.jTable.getSelectedRow();
                this.jTxtId.setText(String.valueOf(this.jTable.getValueAt(fila, 0)));
                this.jTxtNombres.setText(String.valueOf(this.jTable.getValueAt(fila, 1)));
                this.jTxtApellidos.setText(String.valueOf(this.jTable.getValueAt(fila, 2)));
                this.jSpnEdad.setValue(Integer.parseInt(String.valueOf(this.jTable.getValueAt(fila, 3))));
                this.jTxtDui.setText(String.valueOf(this.jTable.getValueAt(fila, 4)));
                this.jTxtTelefono.setText(String.valueOf(this.jTable.getValueAt(fila, 5)));
                this.jTxtDireccion.setText(String.valueOf(this.jTable.getValueAt(fila, 6)));
                this.jTxtLicencia.setText(String.valueOf(this.jTable.getValueAt(fila, 7)));
                if ("Masculino".equals(String.valueOf(this.jTable.getValueAt(fila, 8))))
                {
                    jRdbM.setSelected(true);
                    jRdbF.setSelected(false);
                }
                else if ("Femenino".equals(String.valueOf(this.jTable.getValueAt(fila, 8))))
                {
                    jRdbM.setSelected(false);
                    jRdbF.setSelected(true);
                }
                this.jTxtObservaciones.setText(String.valueOf(this.jTable.getValueAt(fila, 9)));
            }
        }
    }
    // </editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jBtnGrp = new javax.swing.ButtonGroup();
        jLblTitle = new javax.swing.JLabel();
        jLblNombres = new javax.swing.JLabel();
        jTxtNombres = new javax.swing.JTextField();
        jLblDui = new javax.swing.JLabel();
        jTxtDui = new javax.swing.JFormattedTextField();
        jSptDui = new javax.swing.JSeparator();
        jSptNombres = new javax.swing.JSeparator();
        jTxtId = new javax.swing.JTextField();
        jSptApellidos = new javax.swing.JSeparator();
        jLblObservaciones = new javax.swing.JLabel();
        jLblDireccion = new javax.swing.JLabel();
        jSptObservaciones = new javax.swing.JSeparator();
        jTxtObservaciones = new javax.swing.JTextField();
        jTxtDireccion = new javax.swing.JTextField();
        jSptDireccion = new javax.swing.JSeparator();
        jLblEdad = new javax.swing.JLabel();
        jSpnEdad = new javax.swing.JSpinner();
        jSptEdad = new javax.swing.JSeparator();
        jLblApellidos = new javax.swing.JLabel();
        jLblTelefono = new javax.swing.JLabel();
        jTxtTelefono = new javax.swing.JFormattedTextField();
        jSptTelefono = new javax.swing.JSeparator();
        jLblLicencia = new javax.swing.JLabel();
        jTxtLicencia = new javax.swing.JFormattedTextField();
        jSptLicencia = new javax.swing.JSeparator();
        jLblGenero = new javax.swing.JLabel();
        jRdbM = new javax.swing.JRadioButton();
        jRdbF = new javax.swing.JRadioButton();
        jTxtApellidos = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTxtBuscar = new javax.swing.JTextField();
        jSptBuscar = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; // Celdas no editables.
            }
        };
        jPnlMenu = new javax.swing.JPanel();
        jBtnPrint = new javax.swing.JLabel();
        jBtnAdd = new javax.swing.JLabel();
        jBtnEdit = new javax.swing.JLabel();
        jBtnDelete = new javax.swing.JLabel();
        jBtnSave = new javax.swing.JLabel();
        jBtnCancel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(254, 254, 254));
        setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        setMaximumSize(new java.awt.Dimension(820, 530));
        setMinimumSize(new java.awt.Dimension(820, 530));
        setPreferredSize(new java.awt.Dimension(820, 530));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblTitle.setFont(new java.awt.Font("Montserrat", 0, 18)); // NOI18N
        jLblTitle.setForeground(new java.awt.Color(60, 63, 65));
        jLblTitle.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLblTitle.setText("GESTIÓN DE MOTORISTAS");
        add(jLblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 260, 40));

        jLblNombres.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblNombres.setForeground(new java.awt.Color(60, 63, 65));
        jLblNombres.setText("Nombres:");
        add(jLblNombres, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        jTxtNombres.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtNombres.setForeground(new java.awt.Color(60, 63, 65));
        jTxtNombres.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtNombres.setBorder(null);
        jTxtNombres.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTxtNombres.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtNombresKeyTyped(evt);
            }
        });
        add(jTxtNombres, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 200, 20));

        jLblDui.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblDui.setForeground(new java.awt.Color(60, 63, 65));
        jLblDui.setText("Dui:");
        add(jLblDui, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 80, -1, -1));

        jTxtDui.setBorder(null);
        jTxtDui.setForeground(new java.awt.Color(60, 63, 65));
        try {
            jTxtDui.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("########-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTxtDui.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtDui.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        add(jTxtDui, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 100, 20));

        jSptDui.setForeground(new java.awt.Color(48, 103, 158));
        add(jSptDui, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 120, 100, 10));

        jSptNombres.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptNombres, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 200, 10));

        jTxtId.setEditable(false);
        jTxtId.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtId.setForeground(new java.awt.Color(60, 63, 65));
        jTxtId.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtId.setBorder(null);
        jTxtId.setEnabled(false);
        add(jTxtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, 50, 20));

        jSptApellidos.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 120, 200, 10));

        jLblObservaciones.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblObservaciones.setForeground(new java.awt.Color(60, 63, 65));
        jLblObservaciones.setText("Observaciones:");
        add(jLblObservaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, -1, 20));

        jLblDireccion.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblDireccion.setForeground(new java.awt.Color(60, 63, 65));
        jLblDireccion.setText("Dirección:");
        add(jLblDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, -1, 20));

        jSptObservaciones.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptObservaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 780, 10));

        jTxtObservaciones.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtObservaciones.setForeground(new java.awt.Color(60, 63, 65));
        jTxtObservaciones.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtObservaciones.setBorder(null);
        jTxtObservaciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtObservacionesKeyTyped(evt);
            }
        });
        add(jTxtObservaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 780, 20));

        jTxtDireccion.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtDireccion.setForeground(new java.awt.Color(60, 63, 65));
        jTxtDireccion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtDireccion.setBorder(null);
        add(jTxtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 330, 20));

        jSptDireccion.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 330, 10));

        jLblEdad.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblEdad.setForeground(new java.awt.Color(60, 63, 65));
        jLblEdad.setText("Edad:");
        add(jLblEdad, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 80, -1, -1));

        jSpnEdad.setFont(new java.awt.Font("Raleway Medium", 0, 10)); // NOI18N
        jSpnEdad.setModel(new javax.swing.SpinnerNumberModel(18, 18, 70, 1));
        jSpnEdad.setBorder(null);
        add(jSpnEdad, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 100, 60, 20));

        jSptEdad.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptEdad, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 120, 60, 10));

        jLblApellidos.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblApellidos.setForeground(new java.awt.Color(60, 63, 65));
        jLblApellidos.setText("Apellidos:");
        add(jLblApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, -1, -1));

        jLblTelefono.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblTelefono.setForeground(new java.awt.Color(60, 63, 65));
        jLblTelefono.setText("Teléfono:");
        add(jLblTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 80, -1, -1));

        jTxtTelefono.setBorder(null);
        jTxtTelefono.setForeground(new java.awt.Color(60, 63, 65));
        try {
            jTxtTelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTxtTelefono.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtTelefono.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        add(jTxtTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 100, 100, 20));

        jSptTelefono.setForeground(new java.awt.Color(48, 103, 158));
        add(jSptTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 120, 100, 10));

        jLblLicencia.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblLicencia.setForeground(new java.awt.Color(59, 108, 158));
        jLblLicencia.setText("Licencia:");
        add(jLblLicencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 140, -1, -1));

        jTxtLicencia.setBorder(null);
        jTxtLicencia.setForeground(new java.awt.Color(60, 63, 65));
        try {
            jTxtLicencia.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-######-###-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTxtLicencia.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtLicencia.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        add(jTxtLicencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 160, 170, 20));

        jSptLicencia.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptLicencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 180, 170, 10));

        jLblGenero.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblGenero.setForeground(new java.awt.Color(60, 63, 65));
        jLblGenero.setText("Género:");
        add(jLblGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 140, -1, -1));

        jRdbM.setBackground(new java.awt.Color(254, 254, 254));
        jBtnGrp.add(jRdbM);
        jRdbM.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jRdbM.setForeground(new java.awt.Color(60, 63, 65));
        jRdbM.setSelected(true);
        jRdbM.setText("Masculino");
        jRdbM.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        add(jRdbM, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 160, -1, -1));

        jRdbF.setBackground(new java.awt.Color(254, 254, 254));
        jBtnGrp.add(jRdbF);
        jRdbF.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jRdbF.setForeground(new java.awt.Color(60, 63, 65));
        jRdbF.setText("Femenino");
        jRdbF.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        add(jRdbF, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 160, -1, -1));

        jTxtApellidos.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtApellidos.setForeground(new java.awt.Color(60, 63, 65));
        jTxtApellidos.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtApellidos.setBorder(null);
        jTxtApellidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtApellidosKeyTyped(evt);
            }
        });
        add(jTxtApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 100, 200, 20));

        jLabel2.setFont(new java.awt.Font("Quicksand", 0, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(60, 63, 65));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Buscar:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 270, 70, 30));

        jTxtBuscar.setFont(new java.awt.Font("Montserrat", 0, 15)); // NOI18N
        jTxtBuscar.setForeground(new java.awt.Color(60, 63, 65));
        jTxtBuscar.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jTxtBuscar.setToolTipText("Digite el nombre, apellido o DUI del motorista");
        jTxtBuscar.setBorder(null);
        jTxtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtBuscarKeyTyped(evt);
            }
        });
        add(jTxtBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 270, 410, 30));

        jSptBuscar.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 300, 410, 10));

        jScrollPane1.setBackground(new java.awt.Color(252, 252, 252));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jTable.setBackground(new java.awt.Color(252, 252, 252));
        jTable.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jTable.setForeground(new java.awt.Color(60, 63, 65));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"fggadgad", "gadgadg", "adgda", "gadgadg", "adgdagadg", "zcbzcbzcbzc", "zcbzcb", "zcbzcbzb", "cbzbczb"}
            },
            new String [] {
                "ID", "Nombres", "Apellidos", "Edad", "DUI", "Teléfono", "Dirección", "Licencia", "Género"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable.setAlignmentX(2.5F);
        jTable.setAlignmentY(2.5F);
        jTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTable.setGridColor(new java.awt.Color(225, 225, 225));
        jTable.setRowHeight(25);
        jTable.setRowMargin(2);
        jTable.setSelectionBackground(new java.awt.Color(59, 108, 158));
        jTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable.getTableHeader().setResizingAllowed(false);
        jTable.getTableHeader().setReorderingAllowed(false);
        jTable.setUpdateSelectionOnSort(false);
        jTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable);
        if (jTable.getColumnModel().getColumnCount() > 0) {
            jTable.getColumnModel().getColumn(0).setResizable(false);
            jTable.getColumnModel().getColumn(1).setResizable(false);
            jTable.getColumnModel().getColumn(2).setResizable(false);
            jTable.getColumnModel().getColumn(3).setResizable(false);
            jTable.getColumnModel().getColumn(4).setResizable(false);
            jTable.getColumnModel().getColumn(5).setResizable(false);
            jTable.getColumnModel().getColumn(6).setResizable(false);
            jTable.getColumnModel().getColumn(7).setResizable(false);
            jTable.getColumnModel().getColumn(8).setResizable(false);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 780, 190));

        jPnlMenu.setBackground(new java.awt.Color(254, 254, 254));
        jPnlMenu.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));
        jPnlMenu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jBtnPrint.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Print_25px.png"))); // NOI18N
        jBtnPrint.setToolTipText("Imprimir");
        jBtnPrint.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnPrint.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnPrint.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnPrintMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnPrint, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 40, 40));

        jBtnAdd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Plus_25px.png"))); // NOI18N
        jBtnAdd.setToolTipText("Nuevo");
        jBtnAdd.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnAddMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 40));

        jBtnEdit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Edit_25px.png"))); // NOI18N
        jBtnEdit.setToolTipText("Editar");
        jBtnEdit.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnEdit.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnEditMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 40, 40));

        jBtnDelete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Delete_25px.png"))); // NOI18N
        jBtnDelete.setToolTipText("Eliminar");
        jBtnDelete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnDeleteMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 0, 40, 40));

        jBtnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Save_25px.png"))); // NOI18N
        jBtnSave.setToolTipText("Guardar");
        jBtnSave.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnSave.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnSaveMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 0, 40, 40));

        jBtnCancel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Undo_25px.png"))); // NOI18N
        jBtnCancel.setToolTipText("Cancelar");
        jBtnCancel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnCancel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnCancelMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 0, 40, 40));

        add(jPnlMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 260, 240, 40));
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="Eventos de controles">
    private void jTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMouseClicked
        llenarCampos();
    }//GEN-LAST:event_jTableMouseClicked

    private void jBtnAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnAddMouseClicked
        add();
    }//GEN-LAST:event_jBtnAddMouseClicked

    private void jBtnEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnEditMouseClicked
        edit();
    }//GEN-LAST:event_jBtnEditMouseClicked

    private void jBtnDeleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnDeleteMouseClicked
        delete();
    }//GEN-LAST:event_jBtnDeleteMouseClicked

    private void jBtnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnSaveMouseClicked
        save();
    }//GEN-LAST:event_jBtnSaveMouseClicked

    private void jBtnCancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnCancelMouseClicked
        cancel();
    }//GEN-LAST:event_jBtnCancelMouseClicked

    private void jBtnPrintMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnPrintMouseClicked
        print();
    }//GEN-LAST:event_jBtnPrintMouseClicked

    private void jTxtBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtBuscarKeyTyped
        find(jTxtBuscar.getText().replace("'", "''"));
    }//GEN-LAST:event_jTxtBuscarKeyTyped

    private void jTxtNombresKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtNombresKeyTyped
        val.wordsOnly(evt);
    }//GEN-LAST:event_jTxtNombresKeyTyped

    private void jTxtApellidosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtApellidosKeyTyped
        val.wordsOnly(evt);
    }//GEN-LAST:event_jTxtApellidosKeyTyped

    private void jTxtObservacionesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtObservacionesKeyTyped
        val.wordsOnly(evt);
    }//GEN-LAST:event_jTxtObservacionesKeyTyped
    // </editor-fold>

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jBtnAdd;
    private javax.swing.JLabel jBtnCancel;
    private javax.swing.JLabel jBtnDelete;
    private javax.swing.JLabel jBtnEdit;
    private javax.swing.ButtonGroup jBtnGrp;
    private javax.swing.JLabel jBtnPrint;
    private javax.swing.JLabel jBtnSave;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLblApellidos;
    private javax.swing.JLabel jLblDireccion;
    private javax.swing.JLabel jLblDui;
    private javax.swing.JLabel jLblEdad;
    private javax.swing.JLabel jLblGenero;
    private javax.swing.JLabel jLblLicencia;
    private javax.swing.JLabel jLblNombres;
    private javax.swing.JLabel jLblObservaciones;
    private javax.swing.JLabel jLblTelefono;
    private javax.swing.JLabel jLblTitle;
    private javax.swing.JPanel jPnlMenu;
    private javax.swing.JRadioButton jRdbF;
    private javax.swing.JRadioButton jRdbM;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpnEdad;
    private javax.swing.JSeparator jSptApellidos;
    private javax.swing.JSeparator jSptBuscar;
    private javax.swing.JSeparator jSptDireccion;
    private javax.swing.JSeparator jSptDui;
    private javax.swing.JSeparator jSptEdad;
    private javax.swing.JSeparator jSptLicencia;
    private javax.swing.JSeparator jSptNombres;
    private javax.swing.JSeparator jSptObservaciones;
    private javax.swing.JSeparator jSptTelefono;
    private javax.swing.JTable jTable;
    private javax.swing.JTextField jTxtApellidos;
    private javax.swing.JTextField jTxtBuscar;
    private javax.swing.JTextField jTxtDireccion;
    private javax.swing.JFormattedTextField jTxtDui;
    private javax.swing.JTextField jTxtId;
    private javax.swing.JFormattedTextField jTxtLicencia;
    private javax.swing.JTextField jTxtNombres;
    private javax.swing.JTextField jTxtObservaciones;
    private javax.swing.JFormattedTextField jTxtTelefono;
    // End of variables declaration//GEN-END:variables
}
