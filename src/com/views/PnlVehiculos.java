package com.views;

import com.dao.DaoVehiculo;
import com.models.Usuario;
import com.models.Vehiculo;
import com.utils.Validations;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: PnlMotorista
 * Version: 1.0
 * Fecha: 26/8/2018
 * Copyright: ITCA-FEPADE
 * @author Brayan Mejia, Verónica Maribel, Juan Pablo Elias
 */
public class PnlVehiculos extends javax.swing.JPanel
{
    public Boolean adding = false;
    public Boolean editing = false;
    Usuario u = new Usuario();
    Validations val = new Validations();
    DaoVehiculo daoV = new DaoVehiculo();
    Vehiculo veh = new Vehiculo();
    
    public PnlVehiculos(Integer idTu, String user)
    {
        initComponents();
        controls(false);
        all();
        buttons();
        u.setIdTipo(idTu);
        u.setNombre(user);
        jTxtId.setVisible(false);
        adding = false;
        editing = false;
        if (idTu != 1)
        {
            jBtnAdd.setEnabled(false);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(false);
            jBtnCancel.setEnabled(false);
            jBtnPrint.setEnabled(true);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public Boolean existRows()
    {
        Boolean estado = false;
        if (jTable.getRowCount() > 0)
        {
            estado = true;
        }
        return estado;
    }
    
    public void find(String val)
    {
        String[] columnas = {"ID", "Fecha Adquisicion", "En Taller", "Modelo", "Placa", "Marca", 
                "KM Recorridos", "Fecha Últ. Cambio Aceite", "Fecha Últ. Mantenimiento", "Observaciones"};
        Object[] obj = new Object[10];
        DefaultTableModel tabla = new DefaultTableModel(null, columnas);
        List ls;
        try
        {
            ls = daoV.find(val);
            for (int i = 0; i < ls.size(); i++)
            {
                veh = (Vehiculo) ls.get(i);
                obj[0] = veh.getId();
                obj[1] = veh.getFechaAdquisicion();
                obj[2] = veh.getTaller();
                obj[3] = veh.getModelo();
                obj[4] = veh.getPlaca();
                obj[5] = veh.getMarca();
                obj[6] = veh.getKilometrosRecorridos();
                obj[7] = veh.getFechaUltimoCambioAceite();
                obj[8] = veh.getFechaUltimoMantenimiento();
                obj[9] = veh.getObservaciones();
                tabla.addRow(obj);
            }
            this.jTable.setModel(tabla);
            hideColums();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error generando tabla: " + e.toString(), 
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void buttons()
    {
        if (!existRows())
        {
            jBtnAdd.setEnabled(true);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(false);
            jBtnCancel.setEnabled(false);
            jBtnPrint.setEnabled(false);
        }
        else
        {
            jBtnAdd.setEnabled(true);
            jBtnEdit.setEnabled(true);
            jBtnDelete.setEnabled(true);
            jBtnSave.setEnabled(false);
            jBtnCancel.setEnabled(false);
            jBtnPrint.setEnabled(true);
        }
        if (adding || editing)
        {
            jBtnAdd.setEnabled(false);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(true);
            jBtnCancel.setEnabled(true);
            jBtnPrint.setEnabled(false);
        }
    }
    
    public void controls(Boolean option)
    {
        jTxtFechaAd.setEnabled(option);
        jRdbSi.setEnabled(option);
        jRdbNo.setEnabled(option);
        jTxtModelo.setEnabled(option);
        jTxtPlaca.setEnabled(option);
        jTxtMarca.setEnabled(option);
        jTxtKmRec.setEnabled(option);
        jTxtFechaUltCambioA.setEnabled(option);
        jTxtFechaUltMant.setEnabled(option);
        jTxtObservaciones.setEnabled(option);
        jSptFechaAd.setEnabled(option);
        jSptModelo.setEnabled(option);
        jSptPlaca.setEnabled(option);
        jSptMarca.setEnabled(option);
        jSptKmRec.setEnabled(option);
        jSptFechaUltCambioA.setEnabled(option);
        jSptFechaUltMant.setEnabled(option);
        jSptObservaciones.setEnabled(option);
    }
    
    public void all()
    {
        String[] columnas = {"ID", "Fecha Adquisicion", "En Taller", "Modelo", "Placa", "Marca", 
                "KM Recorridos", "Fecha Últ. Cambio Aceite", "Fecha Últ. Mantenimiento", "Observaciones"};
        Object[] obj = new Object[10];
        DefaultTableModel tabla = new DefaultTableModel(null, columnas);
        List ls;
        try
        {
            ls = daoV.all();
            for (int i = 0; i < ls.size(); i++)
            {
                veh = (Vehiculo) ls.get(i);
                obj[0] = veh.getId();
                obj[1] = veh.getFechaAdquisicion();
                obj[2] = veh.getTaller();
                obj[3] = veh.getModelo();
                obj[4] = veh.getPlaca();
                obj[5] = veh.getMarca();
                obj[6] = veh.getKilometrosRecorridos();
                obj[7] = veh.getFechaUltimoCambioAceite();
                obj[8] = veh.getFechaUltimoMantenimiento();
                obj[9] = veh.getObservaciones();
                tabla.addRow(obj);
            }
            this.jTable.setModel(tabla);
            hideColums();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error generando tabla: " + e.toString(), 
                    "Error" + e.getMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void hideColums()
    {
        // Oculta la columna ID
        jTable.getColumnModel().getColumn(0).setMaxWidth(0);
        jTable.getColumnModel().getColumn(0).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        // Oculta la columna Taller
        jTable.getColumnModel().getColumn(2).setMaxWidth(0);
        jTable.getColumnModel().getColumn(2).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(2).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(2).setMinWidth(0);
        // Oculta la columna Modelo
        jTable.getColumnModel().getColumn(3).setMaxWidth(0);
        jTable.getColumnModel().getColumn(3).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(3).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(3).setMinWidth(0);
        // Oculta la columna KM Recorridos
        jTable.getColumnModel().getColumn(6).setMaxWidth(0);
        jTable.getColumnModel().getColumn(6).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);
        // Oculta la columna Observaciones
        jTable.getColumnModel().getColumn(9).setMaxWidth(0);
        jTable.getColumnModel().getColumn(9).setMinWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(9).setMaxWidth(0);
        jTable.getTableHeader().getColumnModel().getColumn(9).setMinWidth(0);
    }
    
    public void add()
    {
        if (!adding && !editing && u.getIdTipo() == 1)
        {
            adding = true;
            buttons();
            controls(true);
            clean();
            jTable.setEnabled(false);
            jTxtObservaciones.setText("Sin observaciones.");
        }
    }
    
    public void edit()
    {
        if (!adding && !editing && u.getIdTipo() == 1)
        {
            if (validacion())
            {
                if (JOptionPane.showConfirmDialog(this, "¿Desea editar el registro?",
                        "Mensaje", JOptionPane.YES_NO_OPTION) == 0)
                {
                    editing = true;
                    buttons();
                    controls(true);
                    jTable.setEnabled(false);
                }
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Seleccione un registro para editar.",
                        "", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public void delete()
    {
        try
        {
            if (!adding && !editing && u.getIdTipo() == 1)
            {
                if (validacion())
                {
                    if (JOptionPane.showConfirmDialog(this, "¿Desea eliminar el registro?",
                            "Mensaje", JOptionPane.YES_NO_OPTION) == 0)
                    {
                        veh.setId(Integer.parseInt(jTxtId.getText()));
                        daoV.delete(veh);
                        all();
                        clean();
                        JOptionPane.showMessageDialog(this, "Registro eliminado exitosamente.",
                            "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Seleccione un registro para eliminar.",
                            "Mensaje", JOptionPane.WARNING_MESSAGE);
                }
                buttons();
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, "Error al eliminar: " + e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void save()
    {
        try
        {
            if (adding || editing)
            {
                if (validacion())
                {
                    if (validDates())
                    {
                        if (adding && !editing)
                        {
                            veh.setId(0);
                            veh.setFechaAdquisicion(jTxtFechaAd.getText());
                            veh.setModelo(jTxtModelo.getText());
                            veh.setPlaca(jTxtPlaca.getText());
                            veh.setMarca(jTxtMarca.getText());
                            veh.setKilometrosRecorridos(Double.parseDouble(jTxtKmRec.getText()));
                            veh.setFechaUltimoCambioAceite(jTxtFechaUltCambioA.getText());
                            veh.setFechaUltimoMantenimiento(jTxtFechaUltMant.getText());
                            veh.setObservaciones(jTxtObservaciones.getText());
                            if (jRdbSi.isSelected())
                                veh.setTaller(true);
                            else if (jRdbNo.isSelected())
                                veh.setTaller(false);
                            daoV.add(veh);
                        }
                        else if (!adding && editing)
                        {
                            veh.setId(Integer.parseInt(jTxtId.getText()));
                            veh.setFechaAdquisicion(jTxtFechaAd.getText());
                            veh.setModelo(jTxtModelo.getText());
                            veh.setPlaca(jTxtPlaca.getText());
                            veh.setMarca(jTxtMarca.getText());
                            veh.setKilometrosRecorridos(Double.parseDouble(jTxtKmRec.getText()));
                            veh.setFechaUltimoCambioAceite(jTxtFechaUltCambioA.getText());
                            veh.setFechaUltimoMantenimiento(jTxtFechaUltMant.getText());
                            veh.setObservaciones(jTxtObservaciones.getText());
                            if (jRdbSi.isSelected())
                                veh.setTaller(true);
                            else if (jRdbNo.isSelected())
                                veh.setTaller(false);
                            daoV.edit(veh);
                        }
                        adding = false;
                        editing = false;
                        controls(false);
                        jTable.setEnabled(true);
                        clean();
                        all();
                        JOptionPane.showMessageDialog(this, "Registro guardado exitosamente.",
                                    "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                        buttons();
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(this, "Ingrese una fecha valida.",
                            "Mensaje", JOptionPane.WARNING_MESSAGE);
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Ingrese los datos obligatorios.",
                            "Mensaje", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, "Error al guardar: " + e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
            buttons();
        }
    }
    
    public void cancel()
    {
        if (adding || editing)
        {
            adding = false;
            editing = false;
            controls(false);
            buttons();
            jTable.setEnabled(true);
            clean();
            all();
        }
    }
    
    public void clean()
    {
        jTxtId.setText("");
        jTxtFechaAd.setText("");
        jTxtModelo.setText("");
        jTxtPlaca.setText("");
        jTxtMarca.setText("");
        jTxtKmRec.setText("");
        jTxtFechaUltCambioA.setText("");
        jTxtFechaUltMant.setText("");
        jTxtObservaciones.setText("");
        jRdbSi.setSelected(false);
        jRdbNo.setSelected(true);
    }
    
    public void print()
    {
        if (!adding && !editing)
        {
            if (validacion())
            {
                if (JOptionPane.showConfirmDialog(this, "¿Desea mostrar el reporte del vehiculo?", 
                    "Imprimir", JOptionPane.YES_NO_OPTION) == 0)
                {
                    daoV.printOne(Integer.parseInt(jTxtId.getText()));
                }
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Seleccione un registro para imprimir.",
                        "", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public Boolean validacion()
    {
        Boolean estado = true;
        if (val.IsNullOrEmpty(jTxtFechaAd.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtModelo.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtPlaca.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtMarca.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtKmRec.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtFechaUltCambioA.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtFechaUltMant.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtObservaciones.getText()))
        {
            estado = false;
        }
        return estado;
    }
    
    public Boolean validDates()
    {
        Boolean estado = true;
        if (!val.isDate(jTxtFechaAd.getText()))
        {
            estado = false;
        }
        if (!val.isDate(jTxtFechaUltCambioA.getText()))
        {
            estado = false;
        }
        if (!val.isDate(jTxtFechaUltMant.getText()))
        {
            estado = false;
        }
        return estado;
    }
    
    public void llenarCampos()
    {
        if (existRows() && !adding && !editing)
        {
            if (this.jTable.getSelectedRow() > -1)
            {
                int fila = this.jTable.getSelectedRow();
                this.jTxtId.setText(String.valueOf(this.jTable.getValueAt(fila, 0)));
                this.jTxtFechaAd.setText(String.valueOf(this.jTable.getValueAt(fila, 1)));
                if (Boolean.parseBoolean(String.valueOf(this.jTable.getValueAt(fila, 2))))
                {
                    jRdbSi.setSelected(true);
                    jRdbNo.setSelected(false);
                }
                else if (!Boolean.parseBoolean(String.valueOf(this.jTable.getValueAt(fila, 2))))
                {
                    jRdbSi.setSelected(false);
                    jRdbNo.setSelected(true);
                }
                this.jTxtModelo.setText(String.valueOf(this.jTable.getValueAt(fila, 3)));
                this.jTxtPlaca.setText(String.valueOf(this.jTable.getValueAt(fila, 4)));
                this.jTxtMarca.setText(String.valueOf(this.jTable.getValueAt(fila, 5)));
                this.jTxtKmRec.setText(String.valueOf(this.jTable.getValueAt(fila, 6)));
                this.jTxtFechaUltCambioA.setText(String.valueOf(this.jTable.getValueAt(fila, 7)));
                this.jTxtFechaUltMant.setText(String.valueOf(this.jTable.getValueAt(fila, 8)));
                this.jTxtObservaciones.setText(String.valueOf(this.jTable.getValueAt(fila, 9)));
            }
        }
    }
    // </editor-fold>
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jBtnGrp = new javax.swing.ButtonGroup();
        jLblTitle = new javax.swing.JLabel();
        jLblFechaAd = new javax.swing.JLabel();
        jTxtFechaAd = new javax.swing.JFormattedTextField();
        jSptFechaAd = new javax.swing.JSeparator();
        jLblModelo = new javax.swing.JLabel();
        jTxtModelo = new javax.swing.JTextField();
        jSptModelo = new javax.swing.JSeparator();
        jLblTaller = new javax.swing.JLabel();
        jRdbSi = new javax.swing.JRadioButton();
        jRdbNo = new javax.swing.JRadioButton();
        jLblKmRec = new javax.swing.JLabel();
        jTxtKmRec = new javax.swing.JTextField();
        jSptKmRec = new javax.swing.JSeparator();
        jLblBuscar = new javax.swing.JLabel();
        jTxtBuscar = new javax.swing.JTextField();
        jSptBuscar = new javax.swing.JSeparator();
        jScrPnl = new javax.swing.JScrollPane();
        jTable = new JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; // Celdas no editables.
            }
        };
        jPnlMenu = new javax.swing.JPanel();
        jBtnPrint = new javax.swing.JLabel();
        jBtnAdd = new javax.swing.JLabel();
        jBtnEdit = new javax.swing.JLabel();
        jBtnDelete = new javax.swing.JLabel();
        jBtnSave = new javax.swing.JLabel();
        jBtnCancel = new javax.swing.JLabel();
        jTxtId = new javax.swing.JTextField();
        jLblPlaca = new javax.swing.JLabel();
        jTxtPlaca = new javax.swing.JTextField();
        jSptPlaca = new javax.swing.JSeparator();
        jLblMarca = new javax.swing.JLabel();
        jTxtMarca = new javax.swing.JTextField();
        jSptMarca = new javax.swing.JSeparator();
        jLblObservaciones = new javax.swing.JLabel();
        jTxtObservaciones = new javax.swing.JTextField();
        jSptObservaciones = new javax.swing.JSeparator();
        jLblFechaUltCambioA = new javax.swing.JLabel();
        jTxtFechaUltCambioA = new javax.swing.JFormattedTextField();
        jSptFechaUltCambioA = new javax.swing.JSeparator();
        jLblFechaUltMant = new javax.swing.JLabel();
        jTxtFechaUltMant = new javax.swing.JFormattedTextField();
        jSptFechaUltMant = new javax.swing.JSeparator();

        setBackground(new java.awt.Color(254, 254, 254));
        setMaximumSize(new java.awt.Dimension(820, 530));
        setMinimumSize(new java.awt.Dimension(820, 530));
        setPreferredSize(new java.awt.Dimension(820, 530));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLblTitle.setFont(new java.awt.Font("Montserrat", 0, 18)); // NOI18N
        jLblTitle.setForeground(new java.awt.Color(60, 63, 65));
        jLblTitle.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLblTitle.setText("GESTIÓN DE VEHÍCULOS");
        add(jLblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 270, 40));

        jLblFechaAd.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblFechaAd.setForeground(new java.awt.Color(60, 63, 65));
        jLblFechaAd.setText("Fecha adquisición:");
        add(jLblFechaAd, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        jTxtFechaAd.setBorder(null);
        jTxtFechaAd.setForeground(new java.awt.Color(60, 63, 65));
        try {
            jTxtFechaAd.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTxtFechaAd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtFechaAd.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        add(jTxtFechaAd, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 140, 20));

        jSptFechaAd.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptFechaAd, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 140, 10));

        jLblModelo.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblModelo.setForeground(new java.awt.Color(60, 63, 65));
        jLblModelo.setText("Modelo:");
        add(jLblModelo, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 80, -1, -1));

        jTxtModelo.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtModelo.setForeground(new java.awt.Color(60, 63, 65));
        jTxtModelo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtModelo.setBorder(null);
        add(jTxtModelo, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 100, 240, 20));

        jSptModelo.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptModelo, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 120, 240, 10));

        jLblTaller.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblTaller.setForeground(new java.awt.Color(60, 63, 65));
        jLblTaller.setText("Taller:");
        add(jLblTaller, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 80, -1, -1));

        jRdbSi.setBackground(new java.awt.Color(254, 254, 254));
        jBtnGrp.add(jRdbSi);
        jRdbSi.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jRdbSi.setForeground(new java.awt.Color(60, 63, 65));
        jRdbSi.setSelected(true);
        jRdbSi.setText("Si");
        jRdbSi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        add(jRdbSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, -1, -1));

        jRdbNo.setBackground(new java.awt.Color(254, 254, 254));
        jBtnGrp.add(jRdbNo);
        jRdbNo.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jRdbNo.setForeground(new java.awt.Color(60, 63, 65));
        jRdbNo.setText("No");
        jRdbNo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        add(jRdbNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 100, -1, -1));

        jLblKmRec.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblKmRec.setForeground(new java.awt.Color(60, 63, 65));
        jLblKmRec.setText("Km Recorridos:");
        add(jLblKmRec, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 140, -1, 20));

        jTxtKmRec.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtKmRec.setForeground(new java.awt.Color(59, 108, 158));
        jTxtKmRec.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtKmRec.setBorder(null);
        jTxtKmRec.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtKmRecKeyTyped(evt);
            }
        });
        add(jTxtKmRec, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 160, 120, 20));

        jSptKmRec.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptKmRec, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 180, 120, 10));

        jLblBuscar.setFont(new java.awt.Font("Quicksand", 0, 16)); // NOI18N
        jLblBuscar.setForeground(new java.awt.Color(60, 63, 65));
        jLblBuscar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLblBuscar.setText("Buscar:");
        add(jLblBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 270, 70, 30));

        jTxtBuscar.setFont(new java.awt.Font("Montserrat", 0, 15)); // NOI18N
        jTxtBuscar.setForeground(new java.awt.Color(60, 63, 65));
        jTxtBuscar.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jTxtBuscar.setBorder(null);
        jTxtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtBuscarKeyTyped(evt);
            }
        });
        add(jTxtBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 270, 410, 30));

        jSptBuscar.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 300, 410, 10));

        jScrPnl.setBackground(new java.awt.Color(252, 252, 252));
        jScrPnl.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jTable.setBackground(new java.awt.Color(252, 252, 252));
        jTable.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jTable.setForeground(new java.awt.Color(60, 63, 65));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Fecha Adquisición", "En Taller", "Modelo", "Placa", "Marca", "KM Recorridos", "Fecha Últ. Cambio Aceite", "Fecha Últ. Mantenimiento", "Observaciones"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable.setAlignmentX(2.5F);
        jTable.setAlignmentY(2.5F);
        jTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTable.setGridColor(new java.awt.Color(225, 225, 225));
        jTable.setRowHeight(25);
        jTable.setRowMargin(2);
        jTable.setSelectionBackground(new java.awt.Color(59, 108, 158));
        jTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable.getTableHeader().setResizingAllowed(false);
        jTable.getTableHeader().setReorderingAllowed(false);
        jTable.setUpdateSelectionOnSort(false);
        jTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMouseClicked(evt);
            }
        });
        jScrPnl.setViewportView(jTable);

        add(jScrPnl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 780, 190));

        jPnlMenu.setBackground(new java.awt.Color(254, 254, 254));
        jPnlMenu.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));
        jPnlMenu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jBtnPrint.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Print_25px.png"))); // NOI18N
        jBtnPrint.setToolTipText("Imprimir");
        jBtnPrint.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnPrint.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnPrint.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnPrintMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnPrint, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 40, 40));

        jBtnAdd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Plus_25px.png"))); // NOI18N
        jBtnAdd.setToolTipText("Nuevo");
        jBtnAdd.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnAddMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 40));

        jBtnEdit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Edit_25px.png"))); // NOI18N
        jBtnEdit.setToolTipText("Editar");
        jBtnEdit.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnEdit.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnEditMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 40, 40));

        jBtnDelete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Delete_25px.png"))); // NOI18N
        jBtnDelete.setToolTipText("Eliminar");
        jBtnDelete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnDeleteMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 0, 40, 40));

        jBtnSave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Save_25px.png"))); // NOI18N
        jBtnSave.setToolTipText("Guardar");
        jBtnSave.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnSave.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnSaveMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 0, 40, 40));

        jBtnCancel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jBtnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/images/Undo_25px.png"))); // NOI18N
        jBtnCancel.setToolTipText("Cancelar");
        jBtnCancel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jBtnCancel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBtnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnCancelMouseClicked(evt);
            }
        });
        jPnlMenu.add(jBtnCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 0, 40, 40));

        add(jPnlMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 260, 240, 40));

        jTxtId.setEditable(false);
        jTxtId.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtId.setForeground(new java.awt.Color(59, 108, 158));
        jTxtId.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtId.setBorder(null);
        jTxtId.setEnabled(false);
        add(jTxtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 20, 50, 20));

        jLblPlaca.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblPlaca.setForeground(new java.awt.Color(60, 63, 65));
        jLblPlaca.setText("Placa:");
        add(jLblPlaca, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 80, -1, -1));

        jTxtPlaca.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtPlaca.setForeground(new java.awt.Color(60, 63, 65));
        jTxtPlaca.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtPlaca.setBorder(null);
        add(jTxtPlaca, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 100, 200, 20));

        jSptPlaca.setForeground(new java.awt.Color(48, 103, 158));
        add(jSptPlaca, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 120, 200, 10));

        jLblMarca.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblMarca.setForeground(new java.awt.Color(60, 63, 65));
        jLblMarca.setText("Marca:");
        add(jLblMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, -1, -1));

        jTxtMarca.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtMarca.setForeground(new java.awt.Color(60, 63, 65));
        jTxtMarca.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtMarca.setBorder(null);
        jTxtMarca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtMarcaKeyTyped(evt);
            }
        });
        add(jTxtMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 210, 20));

        jSptMarca.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 210, 10));

        jLblObservaciones.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblObservaciones.setForeground(new java.awt.Color(60, 63, 65));
        jLblObservaciones.setText("Observaciones:");
        add(jLblObservaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, -1, 20));

        jTxtObservaciones.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        jTxtObservaciones.setForeground(new java.awt.Color(60, 63, 65));
        jTxtObservaciones.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtObservaciones.setBorder(null);
        jTxtObservaciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtObservacionesKeyTyped(evt);
            }
        });
        add(jTxtObservaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 780, 20));

        jSptObservaciones.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptObservaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 780, 10));

        jLblFechaUltCambioA.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblFechaUltCambioA.setForeground(new java.awt.Color(60, 63, 65));
        jLblFechaUltCambioA.setText("Fecha último cambio aceite:");
        add(jLblFechaUltCambioA, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 140, -1, -1));

        jTxtFechaUltCambioA.setBorder(null);
        jTxtFechaUltCambioA.setForeground(new java.awt.Color(59, 108, 158));
        try {
            jTxtFechaUltCambioA.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTxtFechaUltCambioA.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtFechaUltCambioA.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        add(jTxtFechaUltCambioA, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 160, 180, 20));

        jSptFechaUltCambioA.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptFechaUltCambioA, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 180, 180, 10));

        jLblFechaUltMant.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLblFechaUltMant.setForeground(new java.awt.Color(60, 63, 65));
        jLblFechaUltMant.setText("Fecha último mantenimiento:");
        add(jLblFechaUltMant, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 140, -1, -1));

        jTxtFechaUltMant.setBorder(null);
        jTxtFechaUltMant.setForeground(new java.awt.Color(60, 63, 65));
        try {
            jTxtFechaUltMant.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jTxtFechaUltMant.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtFechaUltMant.setFont(new java.awt.Font("Montserrat", 0, 13)); // NOI18N
        add(jTxtFechaUltMant, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 160, 180, 20));

        jSptFechaUltMant.setForeground(new java.awt.Color(60, 63, 65));
        add(jSptFechaUltMant, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 180, 180, 10));
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="Eventos de controles">
    private void jTxtBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtBuscarKeyTyped
        find(jTxtBuscar.getText().replace("'", "''"));
    }//GEN-LAST:event_jTxtBuscarKeyTyped

    private void jTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMouseClicked
        llenarCampos();
    }//GEN-LAST:event_jTableMouseClicked

    private void jBtnPrintMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnPrintMouseClicked
        print();
    }//GEN-LAST:event_jBtnPrintMouseClicked

    private void jBtnAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnAddMouseClicked
        add();
    }//GEN-LAST:event_jBtnAddMouseClicked

    private void jBtnEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnEditMouseClicked
        edit();
    }//GEN-LAST:event_jBtnEditMouseClicked

    private void jBtnDeleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnDeleteMouseClicked
        delete();
    }//GEN-LAST:event_jBtnDeleteMouseClicked

    private void jBtnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnSaveMouseClicked
        save();
    }//GEN-LAST:event_jBtnSaveMouseClicked

    private void jBtnCancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnCancelMouseClicked
        cancel();
    }//GEN-LAST:event_jBtnCancelMouseClicked

    private void jTxtMarcaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtMarcaKeyTyped
        val.wordsOnly(evt);
    }//GEN-LAST:event_jTxtMarcaKeyTyped

    private void jTxtKmRecKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtKmRecKeyTyped
        val.numbersAndPointOnly(evt, jTxtKmRec.getText());
    }//GEN-LAST:event_jTxtKmRecKeyTyped

    private void jTxtObservacionesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtObservacionesKeyTyped
        val.wordsOnly(evt);
    }//GEN-LAST:event_jTxtObservacionesKeyTyped
    // </editor-fold>

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jBtnAdd;
    private javax.swing.JLabel jBtnCancel;
    private javax.swing.JLabel jBtnDelete;
    private javax.swing.JLabel jBtnEdit;
    private javax.swing.ButtonGroup jBtnGrp;
    private javax.swing.JLabel jBtnPrint;
    private javax.swing.JLabel jBtnSave;
    private javax.swing.JLabel jLblBuscar;
    private javax.swing.JLabel jLblFechaAd;
    private javax.swing.JLabel jLblFechaUltCambioA;
    private javax.swing.JLabel jLblFechaUltMant;
    private javax.swing.JLabel jLblKmRec;
    private javax.swing.JLabel jLblMarca;
    private javax.swing.JLabel jLblModelo;
    private javax.swing.JLabel jLblObservaciones;
    private javax.swing.JLabel jLblPlaca;
    private javax.swing.JLabel jLblTaller;
    private javax.swing.JLabel jLblTitle;
    private javax.swing.JPanel jPnlMenu;
    private javax.swing.JRadioButton jRdbNo;
    private javax.swing.JRadioButton jRdbSi;
    private javax.swing.JScrollPane jScrPnl;
    private javax.swing.JSeparator jSptBuscar;
    private javax.swing.JSeparator jSptFechaAd;
    private javax.swing.JSeparator jSptFechaUltCambioA;
    private javax.swing.JSeparator jSptFechaUltMant;
    private javax.swing.JSeparator jSptKmRec;
    private javax.swing.JSeparator jSptMarca;
    private javax.swing.JSeparator jSptModelo;
    private javax.swing.JSeparator jSptObservaciones;
    private javax.swing.JSeparator jSptPlaca;
    private javax.swing.JTable jTable;
    private javax.swing.JTextField jTxtBuscar;
    private javax.swing.JFormattedTextField jTxtFechaAd;
    private javax.swing.JFormattedTextField jTxtFechaUltCambioA;
    private javax.swing.JFormattedTextField jTxtFechaUltMant;
    private javax.swing.JTextField jTxtId;
    private javax.swing.JTextField jTxtKmRec;
    private javax.swing.JTextField jTxtMarca;
    private javax.swing.JTextField jTxtModelo;
    private javax.swing.JTextField jTxtObservaciones;
    private javax.swing.JTextField jTxtPlaca;
    // End of variables declaration//GEN-END:variables
}
