package com.utils;

import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Nombre de la clase: Validations
 * Version: 1.0
 * Fecha: 10/08/2018
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class Validations
{
    public Validations()
    {
        // Nada que hacer
    }
    
    public void numbersOnly(KeyEvent evt)
    {
        if (!Character.isDigit(evt.getKeyChar()))
        {
            evt.consume();
        }
    }
    
    public void numbersAndPointOnly(KeyEvent evt, String val)
    {
        if (!Character.isDigit(evt.getKeyChar()) &&
                (evt.getKeyChar() != '.' || val.contains(".")))
        {
            evt.consume();
        }
    }

    public void wordsOnly(KeyEvent evt)
    {
        if (!Character.isLetter(evt.getKeyChar()) &&
                evt.getKeyChar() != KeyEvent.VK_SPACE)
        {
            evt.consume();
        }
    }

    public boolean IsNullOrEmpty(String val)
    {

        if (val.equals(""))
        {
            return true;
        }
        if (val.length() == 0)
        {
            return true;
        }
        if (val == null)
        {
            return true;
        }
        return false;
    }
    
    public Boolean isDate(String date)
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setLenient(false);
            sdf.parse(date);
        }
        catch (ParseException e)
        {
            return false;
        }
        return true;
    }
}
