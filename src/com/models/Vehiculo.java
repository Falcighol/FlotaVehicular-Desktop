package com.models;

/**
 * Nombre de la clase: Vehiculo
 * Version: 1.0
 * Fecha: 10/08/2018
 * Copyright: ITCA-FEPADE
 * @author Brayan Mejia, Verónica Maribel, Juan Pablo Elias
 */
public class Vehiculo
{
    private int id;
    private String fechaAdquisicion;
    private Boolean taller;
    private String modelo;
    private String placa;
    private String marca;
    private double kilometrosRecorridos;
    private String fechaUltimoCambioAceite;
    private String fechaUltimoMantenimiento;
    private String observaciones;
    private int estado;

    public Vehiculo()
    {
        
    }

    public Vehiculo(int id, String fechaAdquisicion, Boolean taller,
            String modelo, String placa, String marca, double kilometrosRecorridos,
            String fechaUltimoCambioAceite, String fechaUltimoMantenimiento,
            String observaciones, int estado)
    {
        this.id = id;
        this.fechaAdquisicion = fechaAdquisicion;
        this.taller = taller;
        this.modelo = modelo;
        this.placa = placa;
        this.marca = marca;
        this.kilometrosRecorridos = kilometrosRecorridos;
        this.fechaUltimoCambioAceite = fechaUltimoCambioAceite;
        this.fechaUltimoMantenimiento = fechaUltimoMantenimiento;
        this.observaciones = observaciones;
        this.estado = estado;
    }

    public int getEstado()
    {
        return estado;
    }

    public void setEstado(int estado)
    {
        this.estado = estado;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getFechaAdquisicion()
    {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion)
    {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public Boolean getTaller()
    {
        return taller;
    }

    public void setTaller(Boolean taller)
    {
        this.taller = taller;
    }

    public String getModelo()
    {
        return modelo;
    }

    public void setModelo(String modelo)
    {
        this.modelo = modelo;
    }

    public String getPlaca()
    {
        return placa;
    }

    public void setPlaca(String placa)
    {
        this.placa = placa;
    }

    public String getMarca()
    {
        return marca;
    }

    public void setMarca(String marca)
    {
        this.marca = marca;
    }

    public double getKilometrosRecorridos()
    {
        return kilometrosRecorridos;
    }

    public void setKilometrosRecorridos(double kilometrosRecorridos)
    {
        this.kilometrosRecorridos = kilometrosRecorridos;
    }

    public String getFechaUltimoCambioAceite()
    {
        return fechaUltimoCambioAceite;
    }

    public void setFechaUltimoCambioAceite(String fechaUltimoCambioAceite)
    {
        this.fechaUltimoCambioAceite = fechaUltimoCambioAceite;
    }

    public String getFechaUltimoMantenimiento()
    {
        return fechaUltimoMantenimiento;
    }

    public void setFechaUltimoMantenimiento(String fechaUltimoMantenimiento)
    {
        this.fechaUltimoMantenimiento = fechaUltimoMantenimiento;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }
    
}
